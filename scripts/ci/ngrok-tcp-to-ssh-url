#!/usr/bin/env php
<?php
/**
 * @file
 * Decodes YML log output from Ngrok to print an SSH connection URL.
 *
 * Copyright 2023 Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@inveniem.com)
 * @license MIT
 */
// =============================================================================
// Main program
// =============================================================================
try {
  global $argc,
         $argv;

  if ($argc !== 4) {
    $program_name = $argv[0];

    throw new InvalidArgumentException(
      "Usage: $program_name <PATH TO NGROK LOG> <SSH USERNAME> <SSH PASSWORD>",
      1
    );
  }

  $ngrok_log_filename = $argv[1];
  $ssh_username       = $argv[2];
  $ssh_password       = $argv[3];

  $tunnel_uri = determinePublicTunnelUri($ngrok_log_filename);

  if (empty($tunnel_uri)) {
    throw new InvalidArgumentException(
      'Missing or empty public tunnel address.',
      2
    );
  }

  $connection_info =
    rewriteToSshConnectionInfo($tunnel_uri, $ssh_username, $ssh_password);

  print 'You can connect over SSH using the following URI:' . PHP_EOL;
  print PHP_EOL;
  print $connection_info['connection_uri'] . PHP_EOL;
  print PHP_EOL;
  print PHP_EOL;

  print "Or, use the following information if your SSH client doesn't support URLs:" . PHP_EOL;
  print PHP_EOL;
  print 'Host:     ' . $connection_info['hostname'] . PHP_EOL;
  print 'Port:     ' . $connection_info['port'] . PHP_EOL;
  print 'Username: ' . $ssh_username . PHP_EOL;
  print 'Password: ' . $ssh_password . PHP_EOL;
  print PHP_EOL;
}
catch (Exception $ex) {
  $code = $ex->getCode();

  if ($code === 0) {
    $code = 1;
  }

  fwrite(STDERR, $ex->getMessage() . PHP_EOL);
  fwrite(STDERR, PHP_EOL);

  exit($code);
}

// =============================================================================
// Supporting methods
// =============================================================================
/**
 * Locates and parses the public address of an ngrok tunnel from the ngrok log.
 *
 * @param string $ngrok_log_filename
 *   The path to the JSON-format log that ngrok is writing to.
 *
 * @return string|null
 *   The public address of the ngrok tunnel, or NULL if the log does not contain
 *   one.
 */
function determinePublicTunnelUri(string $ngrok_log_filename): ?string {
  $url = NULL;

  if (!is_readable($ngrok_log_filename)) {
    throw new InvalidArgumentException(
      'ngrok log file cannot be read: ' . $ngrok_log_filename
    );
  }

  $log_file_lines = explode(PHP_EOL, file_get_contents($ngrok_log_filename));

  foreach ($log_file_lines as $line) {
    $json_line = json_decode($line, TRUE) ?? [];
    $message = $json_line['msg'] ?? '';

    if ($message === 'started tunnel') {
      $url = $json_line['url'] ?? '';
    }
  }

  return $url;
}


/**
 * Transcribes an ngrok TCP tunnel URI into SSH connection information.
 *
 * @param string $tunnel_uri
 *   The URI of the TCP tunnel being used for SSH
 *   (e.g. "tcp://2.tcp.ngrok.io:16479").
 * @param string $ssh_username
 *   The username to use when connecting over SSH.
 * @param string $ssh_password
 *   The password to use when connecting over SSH.
 *
 * @return array
 *   An associative array with the following keys:
 *   - connection_uri: The SSH connection string/URI, in the format
 *     "ssh://username:password@host:port".
 *   - hostname: The hostname/IP address of the ngrok server hosting the tunnel.
 *   - port: The port on the ngrok server for the tunnel.
 *
 * @throws \InvalidArgumentException
 *   If the URI is not the correct format for a TCP ngrok tunnel.
 */
function rewriteToSshConnectionInfo(string $tunnel_uri,
                                    string $ssh_username,
                                    string $ssh_password): array {
  $tunnel_uri_components = parse_url($tunnel_uri);
  $scheme                = $tunnel_uri_components['scheme'] ?? '';
  $host                  = $tunnel_uri_components['host'] ?? '';
  $port                  = $tunnel_uri_components['port'] ?? '';

  if ($scheme !== 'tcp') {
    throw new InvalidArgumentException(
      'Only TCP tunnel addresses are supported.',
      2
    );
  }

  $connection_string = sprintf(
    '%s://%s:%s@%s:%d',
    'ssh',
    $ssh_username,
    $ssh_password,
    $host,
    $port
  );

  return [
    'connection_uri' => $connection_string,
    'hostname'       => $host,
    'port'           => $port,
  ];
}
