#!/usr/bin/env bash

##
# @file
#   Build the `ngrok-ssh-for-gitlab-ci` Sample Docker image.
#
#   Usage:
#   ./build.sh [Docker image tag]
#
#   If the docker image tag is not provided, it is assumed to be "latest".
#
# Copyright 2023 Inveniem, LLC. All rights reserved.
#
# @author Guy Elsmore-Paddock (guy@inveniem.com)
# @license MIT
#

# Stop on undefined variables, non-zero exit, and if piped commands fail
set -euo pipefail

################################################################################
# Constants
################################################################################
script_path="${BASH_SOURCE[0]}"
script_dirname="$( cd "$( dirname "${script_path}" )" >/dev/null 2>&1 && pwd )"

container_name="php-with-ssh-sample"
registry_host="registry.gitlab.com/guypaddock/ngrok-ssh-for-gitlab-ci"

################################################################################
# Command-line Argument Parsing
################################################################################
tag="${1:-latest}"

if [[ "${tag}" == "latest" ]]; then
  version="dev"
else
  version="${tag}"
fi

################################################################################
# Main Script
################################################################################
cd "${script_dirname}"

vcs_ref=$(git rev-parse --short HEAD)
build_date=$(date --rfc-3339=seconds)
container_name_and_tag="${container_name}:${tag}"

docker build \
  --build-arg "PROJECT_VERSION=${version}" \
  --build-arg "VCS_REF=${vcs_ref}" \
  --build-arg "BUILD_DATE=${build_date}" \
  -t "${container_name_and_tag}" .

docker tag "${container_name_and_tag}" "${registry_host}/${container_name_and_tag}"
docker push "${registry_host}/${container_name_and_tag}"
