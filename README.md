# ngrok SSH for GitLab CI
This project demonstrates how to extend pipeline on GitLab CI to support SSH
access over [ngrok](https://ngrok.com/product) for troubleshooting build
failures.

The sample GitLab CI build in this project will do one of two things, depending
upon whether the `NGROK_AUTH_TOKEN` environment variable has been defined:
- **If the variable has not been defined (normal build):** SSH access will not
  be enabled for the build, and it will run like any other GitLab CI pipeline.
- **If the variable has been defined (SSH debug build):** SSH access will be
  enabled _instead of running the normal build commands_. Information needed to
  connect to the build job over SSH will be printed to the build log, and the
  build will stay running for up to 30 minutes.

You can add these capabilities to your own GitLab CI pipeline by copying the
relevant snippets out of this project into the `Dockerfile` and `.gitlab-ci.yml`
file of your project.

## Trying out the Sample Docker Images and Build Scripts
1.  Ensure you have Docker installed locally.
2.  Ensure you have authenticated your local Docker client [with GitLab's
    Container registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html) service.
3.  [Sign up](https://dashboard.ngrok.com/signup) for a free ngrok account.
4.  Obtain your ngrok auth token (you can find it shown in the
    "2. Connect your account" section of the
    [Getting started page](https://dashboard.ngrok.com/get-started/setup)).
5.  Either fork this repo, or use it as the basis for a new project in your own
    GitLab namespace.
6.  Clone your copy of the repo locally.
7.  In your local copy:
    1. Modify the `REGISTRY_HOST` variable in
       `docker/php-with-ssh-sample/build.sh` to reference the docker registry
       appropriate for your copy of this project on GitLab.
    2.
8.  Open a terminal window.
9.  Change into the `./docker` folder of the local copy of this project.
10. Run `./build.sh latest`.
11. Wait for the docker image to build and become published to GitLab.
12. Inside your copy of the project on GitLab:
    1. Create a new branch called `pipeline-test`.
    2. Create a merge request from your `pipeline-test` branch into _your_ `main`
       branch (e.g., visit `https://gitlab.com/YOUR_NAMESPACE/YOUR_PROJECT/-/merge_requests/new`,
       select `YOUR_PROJECT:pipeline-test` as the source branch, and
       `YOUR_PROJECT:main` as the target branch, then save the MR).
    3. Observe that the merge request pipeline will sleep for 10 seconds and then
       fail with an error message.
    4. Define a new environment variable for your pipeline (under
       `https://gitlab.com/YOUR_NAMESPACE/YOUR_PROJECT/-/settings/ci_cd`), as
       follows:
       - **Key:** `NGROK_AUTH_TOKEN`
       - **Value:** The ngrok auth token you obtained in step 4 above.
       - **Type:** Variable
       - **Environment scope:** `pipeline-test`
       - **Flags:**
         - Mask variable
    5. Re-run the pipeline in your merge request.
    6. Observe that instead of running the normal build steps, you will get
       a message about connecting to the build using SSH, like the following
       example:
       ```
       You can connect over SSH using the following URI:
       ssh://tester:GENERATED_PASSWORD@0.tcp.ngrok.io:12345

       Or, use the following information if your SSH client doesn't support URLs:
       Host:     0.tcp.ngrok.io
       Port:     12345
       Username: tester
       Password: GENERATED_PASSWORD

       <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
       <<<<<<<<<<<<<<<<<<<<<<<<<< SSH IS RUNNING <<<<<<<<<<<<<<<<<<<<<<<<<<<
       <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

       SSH should be running (see connection info above). This session will
       be kept alive for 30 minutes unless stopped manually.
       ```
    7. Open you favorite SSH client and attempt to connect to the build! If
       prompted for a username and password, use the username and password shown
       in the build log.

## Using This in Your Builds
You will need to integrate snippets of code from this sample project into your
CI pipeline. How you do this will depend on how your pipeline currently works:

###  If you are using your own custom Docker images in your current CI pipeline
1. Modify your `Dockerfile` to include the steps from the `Dockerfile` in this
   sample project that install and configure SSH.
2. Publish a new version of your Docker image to the GitLab container registry
   for your project.
3. Modify your `.gitlab-ci.yml` file, as follows:
   1. Reference the new version of your Docker image in the appropriate jobs.
   2. Add the `before_script` snippets that can be invoked to check for an SSH
      key.
   3. Invoke the `before_script` snippets from the `before_script:` sections of
      the parts of your build where you'd like to start SSH in the event that
      an ngrok token has been provided.

###  If you are not using custom Docker images in your current CI pipeline
1. You will need to extend the Docker image(s) you are currently running for
   CI with a custom Docker image. You can do this by defining a `Dockerfile` in
   your project that looks something like this:
   ```Dockerfile
   FROM <YOUR BASE IMAGE>:<VERSION OF BASE IMAGE>
   ```

   For example, if you were previously running in a PHP 8 CLI image:
   ```Dockerfile
   FROM php:8.0-cli-buster
   ```
2. Modify the new custom `Dockerfile` to include the steps from the `Dockerfile`
   in this sample project that install and configure SSH.
3. Publish a new version of your Docker image to the GitLab container registry
   for your project.
4. Modify your `.gitlab-ci.yml` file, as follows:
    1. Reference the new Docker image in the appropriate jobs. This likely means
       changing an `image:` directive.
    2. Add the `before_script` snippets that can be invoked to check for an SSH
       key.
    3. Invoke the `before_script` snippets from the `before_script:` sections of
       the parts of your build where you'd like to start SSH in the event that
       an ngrok token has been provided.

## Security (Is This Safe?)
Please be responsible with your use of SSH on any GitLab runners you do not
control, including GitLab shared runners. This capability is intended only for
troubleshooting or debugging issues with your CI pipeline, not for running
general purpose applications on GitLab infrastructure (e.g., crypto mining,
penetration testing of GitLab resources, etc.). Do not abuse this capability, or
you risk ruining the ability for others to legitimately use this tool to fix
problems.

## Support
Please feel free to file issues in the issue tracker in the original copy of the
project.

## License
MIT License

## Project status
Minimally maintained, seeking co-maintainers.
